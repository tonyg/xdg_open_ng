
import
  std/typetraits, preserves

type
  XdgOpen* {.preservesRecord: "xdg-open".} = object
    `data`*: seq[string]

  Attrs*[E] {.preservesDictionary.} = ref object
    `actions`*: seq[Action]

  UriRunnerConfig*[E] {.preservesRecord: "config".} = ref object
    `attrs`*: Attrs[E]

  Action* {.preservesTuple.} = object
    `pat`*: string
    `cmd`*: seq[string]

proc `$`*[E](x: Attrs[E] | UriRunnerConfig[E]): string =
  `$`(toPreserve(x, E))

proc encode*[E](x: Attrs[E] | UriRunnerConfig[E]): seq[byte] =
  encode(toPreserve(x, E))

proc `$`*(x: XdgOpen | Action): string =
  `$`(toPreserve(x))

proc encode*(x: XdgOpen | Action): seq[byte] =
  encode(toPreserve(x))
