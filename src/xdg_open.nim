# SPDX-FileCopyrightText: ☭ 2021 Emery Hemingway
# SPDX-License-Identifier: Unlicense

import std/[asyncdispatch, os]
import preserves
import syndicate, syndicate/capabilities
import ./protocol

proc unixSocketPath: string =
  result = getEnv("SYNDICATE_SOCK")
  if result == "":
    result = getEnv("XDG_RUNTIME_DIR", "/run/user/1000") / "dataspace"

proc mintCap: SturdyRef =
  var key: array[16, byte]
  mint(key, "syndicate")

type XdgOpen {.preservesRecord: "xdg-open".} = object
  args: seq[string]

bootDataspace("main") do (ds: Ref; turn: var Turn):
  let mainFacet = turn.facet
  connectUnix(turn, unixSocketPath(), mintCap()) do (turn: var Turn; a: Assertion) -> TurnAction:
    let ds = unembed a
    message(turn, ds, XdgOpen(args: commandLineParams()))

for i in 0..7: poll(20)
  # A hack to exit
