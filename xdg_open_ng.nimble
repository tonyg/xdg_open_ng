# Package

version       = "0.1.0"
author        = "Emery"
description   = "A better xdg-open"
license       = "Unlicense"
srcDir        = "src"
bin           = @[ "uri_runner", "xdg_open"]


# Dependencies

requires "nim >= 1.6.4", "syndicate"
